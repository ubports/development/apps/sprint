#include <QDebug>
#include <QFileInfo>

#include "app.h"
#include "applications.h"

// Much of this code js based on similar code from Ubuntu Touch Tweak Tool
// https://bazaar.launchpad.net/~ubuntu-touch-tweak-tool-devs/ubuntu-touch-tweak-tool/reboot/view/head:/src/plugin/tweaktool/desktopfileutils.cpp

static QString keyWithLocale(const QString &key, const QString &locale) {
    return key + QString("[%1]").arg(locale);
}

static QString getLocalizedKey(const QSettings &ini, const QString &key) {
    // https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s04.html
    // http://comments.gmane.org/gmane.comp.window-managers.sawfish/6112

    QString locale = qgetenv("LANG");
    if (locale.isEmpty()) {
        locale = qgetenv("LANGUAGE");
    }

    if (locale.isEmpty()) {
        locale = qgetenv("LC_MESSAGE");
    }

    if (locale.isEmpty()) {
        locale = qgetenv("LC_ALL");
    }

    QString value = ini.value(keyWithLocale(key, locale)).toString();
    if (value.isEmpty()) {
        int n = locale.indexOf("@");
        if (n > -1) {
            locale.truncate(n);
        }

        value = ini.value(keyWithLocale(key, locale)).toString();
    }

    if (value.isEmpty()) {
        int n = locale.indexOf(".");
        if (n > -1) {
            locale.truncate(n);
        }

        value = ini.value(keyWithLocale(key, locale)).toString();
    }

    if (value.isEmpty()) {
        int n = locale.indexOf("_");
        if (n > -1) {
            locale.truncate(n);
        }

        value = ini.value(keyWithLocale(key, locale)).toString();
    }

    if (value.isEmpty()) {
        value = ini.value(key).toString();
    }

    return value;
}

App::App() {}

App::App(const QString desktopFile) :
    m_appInfo(desktopFile, QSettings::IniFormat)
{
    m_desktopFile = desktopFile;

    m_appInfo.setIniCodec("UTF-8");
    
    m_system = m_desktopFile.startsWith(DESKTOP_FILES_FOLDER_SYSTEM);
}

QString App::name() const {
    return getLocalizedKey(m_appInfo, DESKTOP_FILE_KEY_NAME);
}

QString App::comment() const {
    return getLocalizedKey(m_appInfo, DESKTOP_FILE_KEY_COMMENT);
}

QString App::icon() const {
    QString iconName = m_appInfo.value(DESKTOP_FILE_KEY_ICON).toString();

    if (iconName.isEmpty()) {
        return QString();
    }

    if (QFileInfo(iconName).isAbsolute()) {
        return iconName;
    }
    else {
        // Use Ubuntu UITK's UnityThemeIconProvider
        return QString("image://theme/%1").arg(iconName);
    }
}

QString App::appId() const {
    QString appId = m_appInfo.value(DESKTOP_FILE_KEY_APP_ID).toString();

    if (appId.isEmpty()) {
        appId = QFileInfo(m_desktopFile).baseName();
    }

    return appId;
}

QString App::id() const {
    QString id = appId();
    int index = id.lastIndexOf("_");

    if (index >= 0) {
        id.truncate(index);
    }

    return id;
}

QString App::packageName() const {
    QString id = appId();
    int index = id.lastIndexOf("_");

    if (index >= 0) {
        // Remove version
        id.truncate(index);
    }
    
    index = id.lastIndexOf("_");

    if (index >= 0) {
        // Remove app name
        id.truncate(index);
    }

    return id;
}

QString App::version() const {
    QString version = appId();
    int index = version.lastIndexOf("_");

    if (index >= 0) {
        version = version.right(version.size() - index - 1);
    }

    return version;
}

QString App::uri() const {
    return QStringLiteral("application:///%1.desktop").arg(appId());
}

bool App::system() const {
    return m_system;
}

bool App::isVisible() const {
    bool isUbuntuTouchApp = m_appInfo.value(DESKTOP_FILE_KEY_UBUNTU_TOUCH, false).toBool();
    bool shouldNotDisplay = m_appInfo.value(DESKTOP_FILE_KEY_NO_DISPLAY, false).toBool();
    bool shouldDisplayOnUnity = m_appInfo.value(DESKTOP_FILE_KEY_ONLY_SHOW_IN, QStringLiteral("Unity")).toString() == QStringLiteral("Unity");
    bool shouldNotDisplayOnUnity = m_appInfo.value(DESKTOP_FILE_KEY_ONLY_SHOW_IN).toString() == QStringLiteral("Unity");

    if (shouldNotDisplayOnUnity || shouldNotDisplay) {
        return false;
    }

    return isUbuntuTouchApp && shouldDisplayOnUnity;
}
